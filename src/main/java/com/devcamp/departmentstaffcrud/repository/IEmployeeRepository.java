package com.devcamp.departmentstaffcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.departmentstaffcrud.model.CEmployee;

public interface IEmployeeRepository extends JpaRepository<CEmployee, Long>{
    CEmployee findById(long id);
}
