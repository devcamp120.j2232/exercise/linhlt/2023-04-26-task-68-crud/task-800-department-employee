package com.devcamp.departmentstaffcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.departmentstaffcrud.model.CDepartment;

public interface IDepartmentRepository extends JpaRepository<CDepartment, Long>{
    CDepartment findById(long id);
}
