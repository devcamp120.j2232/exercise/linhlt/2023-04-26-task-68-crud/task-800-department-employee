package com.devcamp.departmentstaffcrud.model;

import java.util.Set;
import javax.persistence.*;

@Entity
@Table (name ="department")
public class CDepartment {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;

    @Column (name = "code")
    private String code;

    @Column (name = "name")
    private String name;

    @Column (name = "function")
    private String function;

    @Column (name = "introduction")
    private String introduction;

    @OneToMany (cascade = CascadeType.ALL, mappedBy = "department")
    private Set<CEmployee> employees;
    
    public CDepartment() {
    }

    public CDepartment(long id, String code, String name, String function, String introduction,
            Set<CEmployee> employees) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.function = function;
        this.introduction = introduction;
        this.employees = employees;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Set<CEmployee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<CEmployee> employees) {
        this.employees = employees;
    }
    
    

}
